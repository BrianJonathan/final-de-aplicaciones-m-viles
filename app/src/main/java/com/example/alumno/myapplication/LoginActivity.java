package com.example.alumno.myapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends AppCompatActivity {

    private EditText usernameEt;
    private EditText passwordEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usernameEt = (EditText) findViewById(R.id.username_et);
        passwordEt = (EditText) findViewById(R.id.password_et);
        Button loginBtn = (Button) findViewById(R.id.login_btn);

        if(notLoggedIn()) {
            loginBtn.setOnClickListener(new LoginOnClickListener());
        } else {
            goToEspecialidadList();
        }
    }

    private boolean notLoggedIn() {
        SharedPreferences sharedPreferences = getSharedPreferences(
                getString(R.string.app_name), Context.MODE_PRIVATE);

        return !(sharedPreferences.contains("username") &&
                sharedPreferences.contains("password"));
    }

    public class LoginOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String username = usernameEt.getText().toString();
            String password = passwordEt.getText().toString();
            if(login(username, password)) {
                SharedPreferences sharedPreferences = getSharedPreferences(
                        getString(R.string.app_name), Context.MODE_PRIVATE);
                sharedPreferences.edit()
                        .putString("username", username)
                        .putString("password", password)
                        .commit();
                goToEspecialidadList();
            }
        }
    }

    private void goToEspecialidadList() {
        Intent intent = new Intent(LoginActivity.this, ListActivity.class);
        LoginActivity.this.startActivity(intent);
        finish();
    }

    private boolean login(String username, String password) {
        return username.equals("juan") && password.equals("1234");
    }
}

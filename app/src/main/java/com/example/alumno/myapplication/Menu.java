package com.example.alumno.myapplication;

import java.util.ArrayList;
import java.util.List;

public class Menu {

    private String categoria;
    private List<Plato> platos = new ArrayList<>();

    public Menu() {
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public List<Plato> getPlatos() {
        return platos;
    }

    public void setPlatos(List<Plato> platos) {
        this.platos = platos;
    }
}

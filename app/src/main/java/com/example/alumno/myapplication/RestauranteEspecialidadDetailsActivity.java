package com.example.alumno.myapplication;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

public class RestauranteEspecialidadDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurante_especialidad_details );

        Bundle arguments = getIntent().getExtras();

        Restaurante restaurante = new Gson().fromJson(arguments.getString("restaurante"), Restaurante.class);

        int restauranteImageId = arguments.getInt("imageId");
        String especialidad = arguments.getString("especialidad");
        double direccion = arguments.getDouble("direccion");
        double nombre = arguments.getDouble("nombre");

        ImageView restauranteIv = (ImageView) findViewById(R.id.image);
        TextView especialidadTv = (TextView) findViewById(R.id.especialidad);
        TextView nombreTv = (TextView) findViewById(R.id.nombre);
        TextView direccionTv = (TextView) findViewById(R.id.direccion);

        restauranteIv.setImageResource( restaurante.getImageResourceId());
        especialidadTv.setText( restaurante.getEspecialidad());
        direccionTv.append(String.valueOf( restaurante.getDireccion()));
        nombreTv.append(String.valueOf( restaurante.getNombre()));
    }

}

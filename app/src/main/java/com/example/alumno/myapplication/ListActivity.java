package com.example.alumno.myapplication;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        List<Restaurante> restauranteList = new ArrayList<>();
        initRestauranteList( restauranteList );
        ListView listView = (ListView) findViewById(R.id.restaurante_container);

        listView.setAdapter(new RestauranteAdapter( restauranteList ));
    }

    private void initRestauranteList(List<Restaurante> restauranteList) {

        restauranteList.add(new Restaurante(R.drawable.restaurantedominic, "Pasta", "Balvanera 182", "Dominic"));
        restauranteList.add(new Restaurante(R.drawable.restaurantepiccolino, "Sushi", "9 de julio 1100", "Piccolino"));
        restauranteList.add(new Restaurante(R.drawable.restauranterosset, "Carne", "Viamonte 9001", "Le Rosset"));

    }

    //Método para mostrar los botones de acción
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menuacciones, menu);
        return true;
    }

    //Método para agregar las acciones de nuestros botones
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.compartir){
            Toast.makeText(this, "Compartir", Toast.LENGTH_SHORT).show();
            return true;
        }
        if(id == R.id.buscar){
            Toast.makeText(this, "Buscar", Toast.LENGTH_SHORT).show();
            return true;
        }
        if(id == R.id.opcion1){
            Toast.makeText(this, "Mensaje enviado", Toast.LENGTH_SHORT).show();
            return true;
        }
        if(id == R.id.opcion2){
            Toast.makeText(this, "Cerrar", Toast.LENGTH_SHORT).show();
            return true;
        } return super.onOptionsItemSelected(item);
    }
}

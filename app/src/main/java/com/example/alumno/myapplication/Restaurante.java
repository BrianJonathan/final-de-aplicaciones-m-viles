package com.example.alumno.myapplication;

public class Restaurante {
    private int imageResourceId;
    private String especialidad;
    private String direccion;
    private String nombre;
    

    public Restaurante(int imageResourceId, String especialidad, String direccion, String nombre) {
        this.imageResourceId = imageResourceId;
        this.especialidad = especialidad;
        this.direccion = direccion;
        this.nombre = nombre;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getNombre() {
        return nombre;
    }


}

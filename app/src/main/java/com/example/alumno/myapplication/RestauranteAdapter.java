package com.example.alumno.myapplication;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;


public class RestauranteAdapter extends BaseAdapter{

    List<Restaurante> restaurantes;

    public RestauranteAdapter(List<Restaurante> restauranteList) {
        restaurantes = restauranteList;
    }

    @Override
    public int getCount() {
        return restaurantes.size();
    }

    @Override
    public Object getItem(int position) {
        return restaurantes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return restaurantes.get(position).getEspecialidad().hashCode();
    }

    @Override
    public View getView(int position, View restauranteView, ViewGroup parent) {
        if(restauranteView == null){
            restauranteView = LayoutInflater.from(parent.getContext())
                                    .inflate(R.layout.item_restaurante, parent, false);
        }

        bindRestauranteItem(restauranteView, restaurantes.get(position));

        return restauranteView;
    }

    public void bindRestauranteItem(final View root, final Restaurante restaurante) {
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(root.getContext(),
                        RestauranteEspecialidadDetailsActivity.class);
                intent.putExtra("restaurante", new Gson().toJson( restaurante ));
                root.getContext().startActivity(intent);
            }
        });
        ImageView perroIv = root.findViewById(R.id.image);
        TextView especialidadTv = root.findViewById(R.id.especialidad);
        TextView nombreTv = root.findViewById(R.id.nombre);
        TextView direccionTv = root.findViewById(R.id.direccion);
        perroIv.setImageResource( restaurante.getImageResourceId());
        nombreTv.setText( restaurante.getNombre());
        direccionTv.setText( restaurante.getDireccion());
        especialidadTv.setText( restaurante.getEspecialidad());
    }
}
